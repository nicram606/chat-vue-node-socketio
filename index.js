var express = require('express');
var bodyParser = require('body-parser');
var socket = require('socket.io');
var mongo = require('mongodb');
var app = express();

var serverHelloMessage = {
    id: 0,
    userId: "Server",
    message: "Hello",
    date: new Date()
};

var MongoClient = mongo.MongoClient;
var dbUrl = "*";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.static('public'));

var server = app.listen(5342, function() {
    console.log("Chat api works now on port :5342");
});




//Socket setup
var io = socket(server);
var chat = io.of("/chat");

chat.on('connection', function(socket) {

    socket.on('hi', function(data) {

        var chatId = data.chatId;
        if (chatId == undefined) {
            chatId = 'default';
        }
        socket.join(chatId);

        MongoClient.connect(dbUrl, function(err, db) {
            if (err) {
                console.error(err);
                return;
            }

            db.collection('rooms').find({ 'name': chatId }).toArray(function(err, result) {
                socket.emit('hello', result[0].messages);
                db.close();
            });
        });

        console.log(data.userId + " has joined the chat " + chatId);

    });

    socket.on('chat', function(data) {

        var chatId = data.chatId;
        if (chatId == undefined) {
            chatId = 'default';
        }

        data.date = new Date().getTime();
        data.id = data.userId + data.date;

        chat.to(chatId).emit('chat', data);

        MongoClient.connect(dbUrl, function(err, db) {
            if (err) {
                console.error(err);
                return;
            }

            db.collection('rooms').update({ 'name': chatId }, {
                    "$push": {
                        'messages': {
                            'id': data.id,
                            'userId': data.userId,
                            'message': data.message,
                            'date': new Date()
                        }
                    }
                }).then(function() {
                    console.log("added");
                })
                .catch(function(error) {
                    console.error(error);
                });

            db.close();
        });
    });


    socket.on('typing', function(data) {
        var chatId = data.chatId;
        if (chatId == undefined) {
            chatId = 'default';
        }

        socket.broadcast.to(chatId).emit('typing', data);
    });

    socket.on('roomChange', function(data) {

        console.log(socket.id + " wants to change room from " + data.currentChatId + " to " + data.newChatId);

        MongoClient.connect(dbUrl, function(err, db) {
            if (err) {
                console.error(err);
                return;
            }

            db.collection('rooms').find({ "name": data.newChatId }).toArray(function(err, result) {
                if (err) {
                    console.error(err);
                    socket.emit('roomChange', {
                        status: 'fail',
                        message: "DB error: " + err,
                        chatId: data.newChatId
                    });
                    return;
                }

                if (result.length < 1) {
                    socket.emit('roomChange', {
                        status: 'fail',
                        message: "Given room doesn't exists. Do you want to create it?",
                        chatId: data.newChatId
                    });
                    console.log("but the room " + data.newChatId + " doesn't exist");
                } else {
                    changeRoom(data, socket);
                    console.log("and it's fine");
                }
            });

            db.close();
        });
    });

    socket.on('createRoom', function(data) {
        console.log("creating the room " + data.newChatId);
        MongoClient.connect(dbUrl, function(err, db) {
            if (err) {
                console.error(err);
                return;
            }
            db.collection('rooms').insert({
                    'name': data.newChatId,
                    'messages': [
                        serverHelloMessage
                    ]
                }).then(function() {
                    console.log(data.newChatId + " created and added to DB");
                })
                .catch(function(error) {
                    console.error(error);
                });

            db.close();
            changeRoom(data, socket);
        });
    });

});

function changeRoom(data, socket) {
    socket.leave(data.currentChatId);
    socket.join(data.newChatId);
    socket.emit('roomChange', {
        status: 'success',
        chatId: data.newChatId
    });
    MongoClient.connect(dbUrl, function(err, db) {
        if (err) {
            console.error(err);
            return;
        }

        db.collection('rooms').find({ 'name': data.newChatId }).toArray(function(err, result) {
            socket.emit('hello', result[0].messages);
            db.close();
        });
    });
}